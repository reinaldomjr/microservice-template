/*
 *  TOTVS S/A CONFIDENTIAL
 *  _______________________
 *
 *  2012-2016 (c) TOTVS S/A
 *  All Rights Reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the
 *  property of TOTVS S/A and its suppliers, if any. The intellectual
 *  and technical concepts contained herein are proprietary to TOTVS S/A
 *  and its suppliers and may be covered by U.S. and Foreign Patents,
 *  patents in process, and are protected  by trade secret or copyright
 *  law. Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from TOTVS S/A.
 */

package com.fluig.identity.module.service.application;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fluig.identity.core.application.configuration.RestConfiguration;

import io.dropwizard.Configuration;

public class ModuleConfiguration extends Configuration {

    @JsonProperty
    private RestConfiguration rest;

    public RestConfiguration getRest() {
        return rest;
    }

    public void setRest(RestConfiguration rest) {
        this.rest = rest;
    }
}