package com.fluig.identity.module.service.application;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.glassfish.jersey.servlet.ServletProperties;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fluig.identity.swagger.configuration.SwaggerConfiguration;
import com.fluig.identity.swagger.configuration.SwaggerUI;

import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class ModuleApplication extends Application<ModuleConfiguration> {

    public static void main(String[] args) {
        try {
            new ModuleApplication().run(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(Bootstrap<ModuleConfiguration> bootstrap) {
        super.initialize(bootstrap);
        System.setProperty("java.net.preferIPv4Stack", "true");
        SwaggerUI.initialize(bootstrap);
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                ));
    }

    @Override
    public void run(ModuleConfiguration moduleConfiguration, Environment environment) throws Exception {
        try {
            // Configure TIMESTAMP format
            environment.getObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

            // ADD Configuration Objects to dependency injection
            ServiceLocator locator = ServiceLocatorUtilities.createAndPopulateServiceLocator();
            ServiceLocatorUtilities.addOneConstant(locator, moduleConfiguration);
            ServiceLocatorUtilities.addOneConstant(locator, moduleConfiguration.getRest());
            environment.getApplicationContext().getAttributes().setAttribute(ServletProperties.SERVICE_LOCATOR, locator);

            // Register the service implementations
            environment.jersey().getResourceConfig().packages(true, "com.fluig.identity.module");

            // Register SWAGGER-UI
            SwaggerUI.register(new SwaggerConfiguration(moduleConfiguration.getRest()), environment.jersey());

        } catch (Exception e) {
            throw e;
        }
    }
}
