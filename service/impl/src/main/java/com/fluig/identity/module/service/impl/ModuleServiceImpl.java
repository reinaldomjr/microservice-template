package com.fluig.identity.module.service.impl;

import javax.inject.Inject;
import javax.ws.rs.ext.Provider;

import com.fluig.identity.core.application.configuration.RestConfiguration;
import com.fluig.identity.module.dao.ModuleDAO;
import com.fluig.identity.module.dao.entity.ModuleEntity;
import com.fluig.identity.module.service.ModuleService;
import com.fluig.identity.module.service.application.ModuleConfiguration;
import com.fluig.identity.module.service.dto.ModuleDTO;

@Provider
public class ModuleServiceImpl implements ModuleService {

    @Inject
    private ModuleDAO dao;

    @Inject
    private ModuleConfiguration config;

    @Inject
    private RestConfiguration rest;

    public ModuleDTO doStuff() {
        dao.saveStuff(new ModuleEntity());
        System.err.println(config);
        System.err.println(rest);
        return new ModuleDTO("Stuff Done!");
    }

    public ModuleDTO doStuff(String value) {
        dao.saveStuff(new ModuleEntity());
        return new ModuleDTO("Stuff Done: " + value);
    }
}
