package com.fluig.identity.module.service.dto;

public class ModuleDTO {
    private final String value;

    public ModuleDTO(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
