package com.fluig.identity.module.service;

import org.glassfish.jersey.spi.Contract;

import com.fluig.identity.module.service.dto.ModuleDTO;

@Contract
public interface ModuleService {
    ModuleDTO doStuff();
    ModuleDTO doStuff(String value);
}
