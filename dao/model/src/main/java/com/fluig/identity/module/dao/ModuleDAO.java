package com.fluig.identity.module.dao;

import org.glassfish.jersey.spi.Contract;

import com.fluig.identity.module.dao.entity.ModuleEntity;

@Contract
public interface ModuleDAO {
    void saveStuff(ModuleEntity entity);
}
