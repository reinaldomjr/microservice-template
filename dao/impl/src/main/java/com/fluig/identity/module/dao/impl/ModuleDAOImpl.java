package com.fluig.identity.module.dao.impl;

import javax.ws.rs.ext.Provider;

import com.fluig.identity.module.dao.ModuleDAO;
import com.fluig.identity.module.dao.entity.ModuleEntity;

@Provider
public class ModuleDAOImpl implements ModuleDAO {
    public void saveStuff(ModuleEntity entity) {
        System.err.println("Saving stuff to database");
    }
}
