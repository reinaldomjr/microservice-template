package com.fluig.identity.module.api.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api/module")
public abstract class ModuleRestApplication extends Application{

}
