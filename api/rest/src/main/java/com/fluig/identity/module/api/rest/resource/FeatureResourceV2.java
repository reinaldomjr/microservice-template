package com.fluig.identity.module.api.rest.resource;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fluig.identity.module.service.ModuleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/v2")
@Api(value = "Module Feature V2", description = "Feature Description")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FeatureResourceV2 extends FeatureResourceV1 {

    @Inject
    private ModuleService moduleService;

    @GET
    @Path("/method/{paramName}")
    @ApiOperation(value = "REST Method with params")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized access")
    })
    public Response method2(@ApiParam(name = "paramName", value = "Param Description") @PathParam("paramName") String paramName) {
        return Response.ok(moduleService.doStuff(paramName)).build();
    }

    @GET
    @Path("/method")
    @ApiOperation(value = "REST Method")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized access")
    })
    public Response method() {
        System.err.println("V2");
        return Response.status(Response.Status.NOT_IMPLEMENTED).build();
    }
}
